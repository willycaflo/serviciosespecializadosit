<?php
$nombre = filter_input(INPUT_POST, 'name');
$correo = filter_input(INPUT_POST, 'email');
$asunto = filter_input(INPUT_POST, 'subject');
$mensaje = filter_input(INPUT_POST, 'message');

$body = '<strong>Nombre:</strong>  '.$nombre.'<br/>';
$body .= '<strong>Correo:</strong>  '.$correo.'<br/>';
$body .= '<strong>Mensaje:</strong>  '.$mensaje.'<br/>';

$response = array();

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.ionos.mx';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'info@serviciosespecializadosit.com.mx';                 // SMTP username
    $mail->Password = '2Y!rp]S9je';                           // SMTP password
    $mail->SMTPSecure = 'tls';                         // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    //Recipients
    //$mail->setFrom($correo, $nombre);
    $mail->setFrom('info@serviciosespecializadosit.com.mx',"Contacto sitio web");
    //$mail->addAddress('guillermocaflo@gmail.com', 'Guillermo Cabrera');     // Add a recipient
    $mail->addAddress('i.gonzalez@serviciosespecializadosit.com.mx', 'Ismael Gonzalez');     // Add a recipient

    //Attachments
    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = $asunto;
    $mail->Body    = $body;
    
    if($mail->send()){
        $response['success'] = true;
        $response['message'] = 'Su mensaje ha sido enviado. Gracias!';
    }
} catch (Exception $e) {
    $response['success'] = false;
    $response['message'] = 'Su mensaje no se ha podico enviar. Ocurrio un error: '. $mail->ErrorInfo;
}
echo json_encode($response);
